 
        /*
         * Re embed handlers
         */
    

            /* Dashboard settings button */
            $(".host-actions .settings")
                .click(function() {
                    tictrac.showDashboardSettingsModal();
                    return false;
                })
                .show();

            /* Dashboard share/privacy button */
            $(".host-actions .share")
                .click(function() {
                    tictrac.showDashboardShareModal();
                    return false;
                })
                .show();

            /* Feedback button */
            $(".host-actions .feedback")
                .click(function() {
                    tictrac.showDashboardFeedbackModal();
                    return false;
                })
                .show();

            /* Get dashboard privacy */
            $(".host-actions .privacy")
                .click(function() {
                    tictrac.getDashboardPrivacy(function(privacy) {
                        alert(privacy);
                    });
                    return false;
                })
                .show();

            /* Callback for "reset dashboard" button */
            tictrac.setResetDashboardCallback(function() {
                alert('Reset dashboard callback');
            });

            /* Pass user goals to dashboard code */
            tictrac.setUserGoals([
                'get-fitter',
                'lose-weight'
            ]);
    
